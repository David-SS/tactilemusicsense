var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

//ROUTE ADDED BY ME
//var allSoundsRouter = require('./routes/allSounds');
//var soundRouter = require('./routes/sound');
var getCodeRouter = require('./routes/getCode');
var setCodeOwnerRouter = require('./routes/setCodeOwner');
var getSoundsRouter = require('./routes/getSounds');
var getDefaultSoundsRouter = require('./routes/getDefaultSounds');
var unlinkCodesRouter = require('./routes/unlinkCodes');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

//ROUTE ADDED BY ME
//app.use('/allSounds', allSoundsRouter);
//app.use('/sound', soundRouter);
app.use('/getCode', getCodeRouter);
app.use('/setCodeOwner', setCodeOwnerRouter);
app.use('/getSounds', getSoundsRouter);
app.use('/getDefaultSounds', getDefaultSoundsRouter);
app.use('/unlinkCodes', unlinkCodesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
