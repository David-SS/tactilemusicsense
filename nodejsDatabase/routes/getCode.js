//ROUTE ADDED BY ME (CREATED FILE TO "CALL" IT FOR THIS ROUTE)

var express = require('express');
var router = express.Router();
var url = require('url');

/* GET a code from table... */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host : 'localhost',
        user : 'root',
        password : '',
        database : 'tactilemusicsense'
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query('SELECT code,owner,type from sheet_code where code = "' + queryData.code + '"', function(err, rows, fields) {
        if (!err) {
            if (rows.length > 0) res.send({msg: rows});
            else res.send({msg:[]});
        }
        else res.send({msg:'Access Error'});
    });

    connection.end();
});

module.exports = router;
