//ROUTE ADDED BY ME (CREATED FILE TO "CALL" IT FOR THIS ROUTE)

var express = require('express');
var router = express.Router();
var url = require('url');

/* GET a sound from table by its name... */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host : 'localhost',
        user : 'root',
        password : '',
        database : 'tactilemusicsense'
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query('SELECT audio_file,quadrant from game_sound where type = "' + queryData.type + '"', function(err, rows, fields) {
        if (!err) {
            if (rows.length > 0) res.send({msg: rows});
            else res.send({msg:[]});
        }
        else res.send({msg:'Access Error'});
    });

    connection.end();
});

module.exports = router;
