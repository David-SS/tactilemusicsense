package com.example.david.tactilemusicsense.camera;

import java.io.FileOutputStream;
import java.util.List;

import org.opencv.android.JavaCameraView;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;

public class JavaCameraClass extends JavaCameraView implements PictureCallback {

    private String mPictureFilename;
    private boolean hasFlash;

    public JavaCameraClass(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void takePicture(final String fileName) {
        this.mPictureFilename = fileName;
        mCamera.setPreviewCallback(null);
        mCamera.takePicture(null, null, this);
    }
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        // Write the image in a file (in jpeg format)
        try {
            FileOutputStream fos = new FileOutputStream(mPictureFilename);

            fos.write(data);
            fos.close();

        } catch (java.io.IOException e) {
            Log.e("PictureDemo", "Exception in photoCallback", e);
        }

    }

    public void setHasFlash(boolean hasFlash) {
        this.hasFlash = hasFlash;
    }
    public boolean isFlashEnabled() {
        Camera.Parameters parameters = mCamera.getParameters();
        if (parameters.getFlashMode().equals("torch")) return true;
        else return false;
    }
    public void turnOnFlash() {
        if (this.hasFlash && mCamera != null) {
            Camera.Parameters p = mCamera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            mCamera.setParameters(p);
            mCamera.startPreview();
            doManualFocus();
        }
    }
    public void turnOffFlash() {
        if (this.hasFlash && mCamera != null) {
            Camera.Parameters p = mCamera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(p);
            mCamera.startPreview();
            doManualFocus();
        }
    }

    public void setDefaultAutoFocusMode() {
        if (mCamera != null) {
            Camera.Parameters p = mCamera.getParameters();
            p.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            mCamera.setParameters(p);
            mCamera.startPreview();
        }
    }
    public void doManualFocus() {
        try {
            mCamera.autoFocus(myAutoFocusCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback(){
        @Override
        public void onAutoFocus(boolean arg0, Camera arg1) {
            //if (arg0) mCamera.cancelAutoFocus();
        }
    };

}