package com.example.david.tactilemusicsense.sound;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

public class Metronome implements Serializable{

    private  final static long MILLIS_IN_MINUTE = 60000;
    private int bpm, tempo, tempoPart, waitTempo;
    private boolean metronomeState;

    private boolean photoAtTempo;
    private Context context;

    private Timer mainTimer;
    private MyTimerTask mainTimerTask;

    public Metronome(int bpm, int tempo, int waitTempo, Context context) {
        this.bpm = bpm;
        this.tempo = tempo;
        this.tempoPart = tempo;
        this.waitTempo = waitTempo;
        this.metronomeState = false;
        this.photoAtTempo = false;
        this.context = context;
    }

    public boolean isMetronomeState() {
        return metronomeState;
    }
    public boolean isPhotoAtTempo() { return photoAtTempo; }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }
    public void setTempo(int tempo) {
        this.tempo = tempo;
        this.tempoPart = tempo;
    }
    public void setWaitTempo(int waitTempo) { this.waitTempo = waitTempo; }
    public void setPhotoAtTempo(boolean photoAtTempo) { this.photoAtTempo = photoAtTempo; }

    public void playMetronome() {
        mainTimer = new Timer();
        mainTimerTask = new MyTimerTask();
        mainTimer.schedule(mainTimerTask, 0, this.MILLIS_IN_MINUTE / this.bpm);
        this.metronomeState = true;
    }
    public void stopMetronome() {
        if (mainTimer != null && mainTimerTask != null) {
            mainTimer.cancel();
            mainTimerTask.cancel();
        }
        this.tempoPart = tempo;
        this.metronomeState = false;
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            playSound();
        }
    }
    private void playSound() {
        this.tempoPart--;
        if (this.tempoPart == 0) {
            this.tempoPart = this.tempo + this.waitTempo;
            this.photoAtTempo = true;
        }
        else if (tempoPart < this.tempo){
            Intent plays = new Intent(context, PlaySound.class);
            plays.putExtra("sound","/compas.mp3");
            context.startService(plays);
        }
    }
}
