package com.example.david.tactilemusicsense.imageProcessing;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.ExecutionException;

public class ProcessImage {

    private static String rootPictureFolder;
    private static String reference;
    private static String comparator;
    private static double sensitivity;

    public static void init(Context context) {
        ProcessImage.rootPictureFolder = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        ProcessImage.reference = "REFERENCE.jpg";
        ProcessImage.comparator = "COMPARATOR.jpg";
        ProcessImage.sensitivity = 25.0;
    }
    public static String getFilePath(String filename) {
        return rootPictureFolder + "/" + filename;
    }
    public static void setSensitivity(double sensitivity) {
        ProcessImage.sensitivity = sensitivity;
    }

    public static boolean[] checkInteraction() {
        //Read images
        Mat referencePicture = Imgcodecs.imread(getFilePath(reference));
        Mat comparatorPicture = Imgcodecs.imread(getFilePath(comparator));

        //Obtain 4 segments from both images (reference and comparator)
        Mat[] referenceResult = fourSegmentImage(referencePicture);
        Mat[] comparatorResult = fourSegmentImage(comparatorPicture);

        //Save four segmented image
        for (int i=0;i<4;i++) Imgcodecs.imwrite(getFilePath("/"+i+".jpg"), referenceResult[i]);

        //Run thread to calculate interaction
        AsyncTask<Object, Integer, Boolean> result0;
        AsyncTask<Object, Integer, Boolean> result1;
        AsyncTask<Object, Integer, Boolean> result2;
        AsyncTask<Object, Integer, Boolean> result3;
        result0 = new AsyncImageInteraction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,referenceResult[0],comparatorResult[0],sensitivity);
        result1 = new AsyncImageInteraction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,referenceResult[1],comparatorResult[1],sensitivity);
        result2 = new AsyncImageInteraction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,referenceResult[2],comparatorResult[2],sensitivity);
        result3 = new AsyncImageInteraction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,referenceResult[3],comparatorResult[3],sensitivity);

        //Prepare interaction detect results
        boolean[] interaction = {false, false, false, false};
        try {
            if (result0.get()) interaction[0] = true;
            if (result1.get()) interaction[1] = true;
            if (result2.get()) interaction[2] = true;
            if (result3.get()) interaction[3] = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return interaction;
    }
    private static Mat[] fourSegmentImage(Mat original) {
        Point topLeft, bottomLeft, topRight, bottomRight;
        topLeft = new Point(0,0);
        bottomLeft = new Point(0,original.height()/2);
        topRight = new Point(original.width()/2,0);
        bottomRight = new Point(original.width()/2,original.height()/2);

        int height = original.height()/2;
        int width = original.width()/2;

        Mat[] imageOutput = new Mat[4];

        Rect rectCrop = new Rect((int)topLeft.x,(int)topLeft.y,width,height);
        imageOutput[0] = original.submat(rectCrop);

        rectCrop = new Rect((int)bottomLeft.x,(int)bottomLeft.y,width,height);
        imageOutput[1] = original.submat(rectCrop);

        rectCrop = new Rect((int)topRight.x,(int)topRight.y,width,height);
        imageOutput[2] = original.submat(rectCrop);

        rectCrop = new Rect((int)bottomRight.x,(int)bottomRight.y,width,height);
        imageOutput[3] = original.submat(rectCrop);

        return imageOutput;
    }

    public static void takeReferencePicture(Mat imageToSave) {
        Mat adjustedImage = new Mat(imageToSave.size(), imageToSave.type());
        Imgproc.cvtColor(imageToSave, adjustedImage, Imgproc.COLOR_BGR2RGBA);
        cropImageToSquare(adjustedImage,getFilePath(reference));
    }
    public static void takeComparatorPicture(Mat imageToSave) {
        Mat adjustedImage = new Mat(imageToSave.size(), imageToSave.type());
        Imgproc.cvtColor(imageToSave, adjustedImage, Imgproc.COLOR_BGR2RGBA);
        cropImageToSquare(adjustedImage,getFilePath(comparator));
    }
    private static void cropImageToSquare(Mat imageToCrop, String filenameDestPath) {
        int inputWidth=imageToCrop.width(), inputHeight=imageToCrop.height();
        if (inputWidth < inputHeight) {
            Point upCenter = new Point(0,inputHeight/2-inputWidth/2);
            Rect rectCrop = new Rect((int)upCenter.x,(int)upCenter.y,(inputWidth),(inputWidth));
            imageToCrop = imageToCrop.submat(rectCrop);
            Imgcodecs.imwrite(filenameDestPath,imageToCrop);
        } else if (inputHeight < inputWidth) {
            Point upCenter = new Point(inputWidth/2-inputHeight/2,0);
            Rect rectCrop = new Rect((int)upCenter.x,(int)upCenter.y,(inputHeight),(inputHeight));
            imageToCrop = imageToCrop.submat(rectCrop);
            Imgcodecs.imwrite(filenameDestPath,imageToCrop);
        }
    }

    public static void drawCross(Mat mRgba) {
        Rect rect = new Rect();
        rect.width = mRgba.width();
        rect.height = mRgba.height();

        int x = (int) (rect.tl().x + rect.br().x)/2;
        int y = (int) (rect.tl().y + rect.br().y)/2;

        int width = 0;
        int height = 110;
        Rect rect1 = new Rect(x - width / 2, y - height / 2, width, height);
        Imgproc.rectangle(mRgba, rect1.tl(), rect1.br(), new Scalar(255, 255, 255), 2, 8, 0);
        width = height;
        height = 0;
        rect1 = new Rect(x - width / 2, y - height / 2, width, height);
        Imgproc.rectangle(mRgba, rect1.tl(), rect1.br(), new Scalar(255, 255, 255), 2, 8, 0);
    }
    public static void drawSquare(Mat mRgba) {
        int inputWidth=mRgba.width(), inputHeight=mRgba.height();
        if (inputWidth < inputHeight) {
            Point upCenter = new Point(0,inputHeight/2-inputWidth/2);
            Rect rectCrop = new Rect((int)upCenter.x,(int)upCenter.y,(inputWidth),(inputWidth));
            Imgproc.rectangle(mRgba, rectCrop.tl(), rectCrop.br(), new Scalar(255, 255, 255), 2, 8, 0);
        }
        else if (inputHeight < inputWidth) {
            Point upCenter = new Point(inputWidth/2-inputHeight/2,0);
            Rect rectCrop = new Rect((int)upCenter.x,(int)upCenter.y,(inputHeight),(inputHeight));
            Imgproc.rectangle(mRgba, rectCrop.tl(), rectCrop.br(), new Scalar(255, 255, 255), 2, 8, 0);
        }
    }
}
