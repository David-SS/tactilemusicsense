package com.example.david.tactilemusicsense.imageProcessing;

import android.os.AsyncTask;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class AsyncImageInteraction extends AsyncTask<Object, Integer, Boolean> {
    @Override
    protected Boolean doInBackground(Object... objects) {
        //Images for determine if there is any interaction.
        Mat referenceResult = (Mat) objects[0];
        Mat comparatorResult = (Mat)objects[1];
        Mat difference = Mat.zeros(referenceResult.rows(),referenceResult.cols(),CvType.CV_8UC1);

        //Sensitivity
        double sensitivity = (double)objects[2];

        //Absolute diff operation
        Core.absdiff(referenceResult, comparatorResult, difference);

        //Threshold to determine if a pixel is considerate "different" and the count for different pixels
        int threshold = 80;
        float differentPixels = 0;

        //Variables to NOT ANALIZE ALL PIXELS
        int ignoredPixels = 3;
        int totalIgnoredPixels = 0;
        int rowIteration = -1;
        int colIteration = -1;
        int i, j;

        for (i = 0; i < difference.rows(); i=i+1+ignoredPixels) {
            rowIteration++;
            for (j = 0; j < difference.cols(); j=j+1+ignoredPixels) {
                colIteration++;
                double[] pix = difference.get(i,j);
                int val = (int) (pix[0] + pix[1] + pix[2]);
                if (val > threshold) differentPixels++;
            }
            // total + (ignored column pixels in actual row) + (ignored column pixels which do not
            //  not have not ignored pixels after them in actual row).
            totalIgnoredPixels += (ignoredPixels * colIteration) + (difference.cols()-(j-ignoredPixels));
            colIteration=-1;
        }
        // total + (total row pixels ignored) + (total row pixels ignored after not ignored rows)
        totalIgnoredPixels += (ignoredPixels*difference.cols()*rowIteration) + ((difference.rows()-(i-ignoredPixels))*difference.cols());

        System.out.println("PORCENTAJE DE DIFERENCIA = " + (differentPixels/(difference.rows()*difference.cols()-totalIgnoredPixels))*100);
        if ((differentPixels/(difference.rows()*difference.cols()-totalIgnoredPixels))*100 > sensitivity) return true;
        return false;
    }
}
