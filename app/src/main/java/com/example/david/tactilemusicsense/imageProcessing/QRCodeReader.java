package com.example.david.tactilemusicsense.imageProcessing;

import android.graphics.Bitmap;
import com.google.zxing.*;
import com.google.zxing.common.HybridBinarizer;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

public class QRCodeReader {
    public static String decodeQRCode(Mat codeImage) {
        Bitmap bMap = Bitmap.createBitmap(codeImage.width(), codeImage.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(codeImage, bMap);
        int[] intArray = new int[bMap.getWidth()*bMap.getHeight()];

        bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(), bMap.getHeight());
        LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(), bMap.getHeight(),intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            return null;
        }
    }
}
