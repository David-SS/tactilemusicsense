package com.example.david.tactilemusicsense.database;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.david.tactilemusicsense.activities.GameActivity;
import com.example.david.tactilemusicsense.encrypt.MD5;

import org.json.JSONObject;

public class CodeUnlinker extends Service {

    public CodeUnlinker() {}

    @Override
    public void onCreate() { super.onCreate(); }

    @Override  //Deprecated.. see newest implementation
    public void onStart(Intent intent, int flags) {
        if (intent != null) resetCodeOwner();
    }

    public void resetCodeOwner() {
        String owner = Settings.Secure.getString(this.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        AQuery aq = new AQuery(this);
        String url = "http://dvdhack.ddns.net:3000/unlinkCodes?owner="+ MD5.encrypt(owner);
        aq.ajax(url, JSONObject.class, this, "unlinkOwnerCallback");
    }
    public void unlinkOwnerCallback (String url, JSONObject json, AjaxStatus status) {
        if (json != null) {
            Toast.makeText( this,"Ahora puedes vincular todos tus códigos en otro dispositivo",Toast.LENGTH_LONG).show();
            sendMessageToActivity();
        }
        else Toast.makeText(this,"QUERY ERROR - UNLINK CODES",Toast.LENGTH_LONG).show();
    }
    private void sendMessageToActivity() {
        Intent sendResult = new Intent();
        sendResult.setAction(GameActivity.SERVICE_UNLINK_CODES_ACTION);
        sendBroadcast(sendResult);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
