package com.example.david.tactilemusicsense.database;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.david.tactilemusicsense.activities.GameActivity;
import com.example.david.tactilemusicsense.encrypt.MD5;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SoundDownloader extends Service {

    private String rootSoundFolder;

    public SoundDownloader() {}

    @Override
    public void onCreate() {
        super.onCreate();
        rootSoundFolder = getExternalFilesDir(Environment.DIRECTORY_MUSIC).getAbsolutePath();
    }

    @Override  //Deprecated.. see newest implementation
    public void onStart(Intent intent, int flags) {
        if (intent != null) {
            getDefaultSounds();
            getCode(intent);
        }
    }

    private void getDefaultSounds() {
        AQuery aq = new AQuery(this);
        String url = "http://dvdhack.ddns.net:3000/getDefaultSounds";
        aq.ajax(url, JSONObject.class, this, "defaultSoundsCallback");
    }
    public void defaultSoundsCallback (String url, JSONObject json, AjaxStatus status) {
        try {
            JSONArray sounds = json.getJSONArray("msg");
            if (sounds.length() > 0) {
                for (int i=0; i<sounds.length();i++) {
                    String quadrant = sounds.getJSONObject(i).getString("quadrant");
                    if (Integer.parseInt(quadrant) == -1) saveAudioFile(sounds.getJSONObject(i), "compas");
                    if (Integer.parseInt(quadrant) == -4) saveAudioFile(sounds.getJSONObject(i), "wrong_interaction");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCode(Intent intent) {
        String code = (String) intent.getSerializableExtra("code");
        AQuery aq = new AQuery(this);
        String url = "http://dvdhack.ddns.net:3000/getCode?code="+Integer.parseInt(code);
        aq.ajax(url, JSONObject.class, this, "codeDBCallback");
    }
    public void codeDBCallback (String url, JSONObject json, AjaxStatus status) {
        if (json != null) checkQRCodeOwner(json);
        else Toast.makeText(this,"QUERY ERROR - GET CODE",Toast.LENGTH_LONG).show();
    }
    private void checkQRCodeOwner(JSONObject json) {
        Toast.makeText( this,"Verificando..",Toast.LENGTH_SHORT).show();
        try {
            JSONArray jsonResponse = json.getJSONArray("msg");
            if (jsonResponse.length() == 0) Toast.makeText( this,"Codigo no válido",Toast.LENGTH_SHORT).show();
            else {
                String uuid = Settings.Secure.getString(this.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                String owner = jsonResponse.getJSONObject(0).getString("owner");
                String code = jsonResponse.getJSONObject(0).getString("code");
                String type = jsonResponse.getJSONObject(0).getString("type");
                if (jsonResponse.getJSONObject(0).isNull("owner") || owner.equals("")) {
                    Toast.makeText( this,"Registrando codigo..",Toast.LENGTH_SHORT).show();
                    setCodeOwner(MD5.encrypt(uuid),code);
                    getSounds(type);
                }
                else {
                    if (owner.equals(MD5.encrypt(uuid))) {
                        Toast.makeText( this,"Codigo propietario correcto",Toast.LENGTH_SHORT).show();
                        getSounds(type);
                    }
                    else Toast.makeText( this,"Este codigo ya se ha registrado",Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setCodeOwner(String owner, String code) {
        AQuery aq = new AQuery(this);
        String url = "http://dvdhack.ddns.net:3000/setCodeOwner?owner="+ owner+"&code="+Integer.parseInt(code);
        aq.ajax(url, JSONObject.class, this, "setCodeOwnerCallback");
    }
    public void setCodeOwnerCallback (String url, JSONObject json, AjaxStatus status) {
        if (json != null) Toast.makeText( this,"Ahora eres propiertario de este codigo",Toast.LENGTH_LONG).show();
        else Toast.makeText(this,"QUERY ERROR - SET CODE",Toast.LENGTH_LONG).show();
    }

    private void getSounds(String type) {
        AQuery aq = new AQuery(this);
        String url = "http://dvdhack.ddns.net:3000/getSounds?type="+Integer.parseInt(type);
        aq.ajax(url, JSONObject.class, this, "getSoundsCallback");
    }
    public void getSoundsCallback (String url, JSONObject json, AjaxStatus status) {
        try {
            JSONArray sounds = json.getJSONArray("msg");
            if (sounds.length() == 0) Toast.makeText( this,"No existen sonidos para este tipo de lámina",Toast.LENGTH_SHORT).show();
            else {
                Toast.makeText( this,"Descargando y activando contenido..",Toast.LENGTH_SHORT).show();
                for (int i=0; i<sounds.length();i++) {
                    String quadrant = sounds.getJSONObject(i).getString("quadrant");
                    saveAudioFile(sounds.getJSONObject(i), "sound"+quadrant);
                }
                Toast.makeText( this,"Descarga completada",Toast.LENGTH_SHORT).show();
                sendMessageToActivity();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveAudioFile(JSONObject sound, String soundName) {
        byte[] soundByteArray = getByteAudioData(sound);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(rootSoundFolder + "/" + soundName + ".mp3");
            fos.write(soundByteArray);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private byte[] getByteAudioData(JSONObject sound) {
        byte[] audioBytes = null;
        try {
            JSONObject audio_file = sound.getJSONObject("audio_file");
            JSONArray jsonArrayBytes = new JSONArray(audio_file.getString("data"));
            audioBytes = new byte[jsonArrayBytes.length()];
            for (int i = 0; i < jsonArrayBytes.length(); i++) {
                audioBytes[i] = (byte) jsonArrayBytes.getInt(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return audioBytes;
    }
    private void sendMessageToActivity() {
        Intent sendResult = new Intent();
        sendResult.setAction(GameActivity.SERVICE_GET_SOUNDS_ACTION);
        sendResult.putExtra(GameActivity.SERVICE_GET_SOUNDS_DATA, true);
        sendBroadcast(sendResult);
    }

    @Override
    public void onDestroy() { super.onDestroy(); }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
