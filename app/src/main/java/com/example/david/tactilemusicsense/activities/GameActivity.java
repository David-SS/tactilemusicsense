package com.example.david.tactilemusicsense.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.example.david.tactilemusicsense.R;
import com.example.david.tactilemusicsense.activities.preferences.PreferencesFragment;
import com.example.david.tactilemusicsense.activities.preferences.SetPreferencesActivity;
import com.example.david.tactilemusicsense.camera.JavaCameraClass;
import com.example.david.tactilemusicsense.database.CodeUnlinker;
import com.example.david.tactilemusicsense.permission.ApplicationPermission;
import com.example.david.tactilemusicsense.imageProcessing.ProcessImage;
import com.example.david.tactilemusicsense.imageProcessing.QRCodeReader;
import com.example.david.tactilemusicsense.database.SoundDownloader;
import com.example.david.tactilemusicsense.sound.Metronome;
import com.example.david.tactilemusicsense.sound.PlaySound;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class GameActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private Metronome metronome;
    private JavaCameraClass mOpenCvCameraView;
    private Mat mRgba;
    private int takenPicturesNum;
    private boolean viewEnabled;
    private boolean checkCodeAction,buttonEnabled;
    private int checkCodeAttempts, maxCheckCodeAttempts;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("viewEnabled", this.viewEnabled);
        outState.putBoolean("buttonEnabled", this.buttonEnabled);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getSavedInstanceState(savedInstanceState);
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_game);
    }
    private void getSavedInstanceState(Bundle savedInstanceState) {
        this.buttonEnabled = false;
        this.viewEnabled = true;
        if (savedInstanceState != null) {
            if (!savedInstanceState.getBoolean("viewEnabled")) {
                this.viewEnabled = false;
                setTheme(R.style.AppThemeHidden);
            }
            this.buttonEnabled = savedInstanceState.getBoolean("buttonEnabled");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        this.metronome.stopMetronome();
        this.mOpenCvCameraView.turnOffFlash();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    @Override
    public void onPause() {
        super.onPause();
        this.metronome.stopMetronome();
        this.mOpenCvCameraView.turnOffFlash();
        this.stopGame();
        unregisterReceiver(mReceiver);
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
        if (!OpenCVLoader.initDebug())
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        else
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
    }

    private void init() {
        ApplicationPermission.init(this);
        ProcessImage.init(this);
        metronomeInit();
        intentFilterInit();
        checkCodeVarInit();
        openCvCameraInit();
        toggleButtonInit();
    }
    private void intentFilterInit() {
        this.mIntentFilter = new IntentFilter();
        this.mIntentFilter.addAction(SERVICE_GET_SOUNDS_ACTION);
        this.mIntentFilter.addAction(SERVICE_UNLINK_CODES_ACTION);
    }
    private void checkCodeVarInit() {
        this.checkCodeAttempts = 0;
        this.maxCheckCodeAttempts = 50;
        this.checkCodeAction = false;
    }
    private void metronomeInit() {
        this.metronome = new Metronome(60, 4, 0, getApplicationContext());
    }
    private void openCvCameraInit() {
        mOpenCvCameraView = (JavaCameraClass) findViewById(R.id.javacameraclass_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setAlpha(0);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setHasFlash(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH));
        this.takenPicturesNum = 0;
    }
    private void toggleButtonInit() {
        final ToggleButton playButton = (ToggleButton) findViewById(R.id.playGameButton);
        playButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) play();
                else stop();
            }
        });
        playButton.setEnabled(this.buttonEnabled);

        findViewById(R.id.gameLayout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction() == 1 && playButton.isEnabled())
                    playButton.setChecked(!playButton.isChecked());
                return true;
            }
        });
    }

    public void play(){
        setSettings();
        this.takenPicturesNum = 0;
        if (this.metronome.isMetronomeState()) this.metronome.stopMetronome();
        this.metronome.playMetronome();
    }
    public void setSettings() {
        String bpm = PreferencesFragment.getString(this,PreferencesFragment.KEY_BPM);
        String tempo = PreferencesFragment.getString(this,PreferencesFragment.KEY_TEMPO);
        String waitTempo = PreferencesFragment.getString(this,PreferencesFragment.KEY_WAIT_TEMPO);
        String sensitivity = PreferencesFragment.getString(this,PreferencesFragment.KEY_SENSITIVITY);
        Boolean flash = PreferencesFragment.getBoolean(this,PreferencesFragment.KEY_FLASH);

        if (isEmptyString(bpm)) this.metronome.setBpm(Integer.parseInt(bpm));
        if (isEmptyString(tempo)) this.metronome.setTempo(Integer.parseInt(tempo));
        if (isEmptyString(waitTempo)) this.metronome.setWaitTempo(Integer.parseInt(waitTempo));
        if (isEmptyString(sensitivity)) ProcessImage.setSensitivity(Double.parseDouble(sensitivity));
        if (flash) mOpenCvCameraView.turnOnFlash();
    }
    private boolean isEmptyString(String inputText) {
        return inputText != null && !inputText.equals("");
    }
    public void stop(){
        this.metronome.stopMetronome();
        if (this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH))
            mOpenCvCameraView.turnOffFlash();
    }

    /**
     * Allow users to play the game (after scan a correct code) or block the game (unlink codes)
     */
    public void turnOnGame() {
        this.buttonEnabled = true;
        ToggleButton playButton = (ToggleButton) findViewById(R.id.playGameButton);
        playButton.setEnabled(true);
    }
    public void turnOffGame() {
        this.buttonEnabled = false;
        ToggleButton playButton = (ToggleButton) findViewById(R.id.playGameButton);
        playButton.setEnabled(false);
    }
    public void stopGame() {
        ToggleButton playButton = (ToggleButton) findViewById(R.id.playGameButton);
        playButton.setChecked(false);
    }


    /**
     * Camera events and processing [Detect code, play sounds if detects interaction, etc]
     */
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    mOpenCvCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mOpenCvCameraView.setDefaultAutoFocusMode();
        mOpenCvCameraView.doManualFocus();
    }
    public void onCameraViewStopped() { mRgba.release(); }
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        if (this.checkCodeAction) findCodeOnImage(mRgba);
        else if (this.metronome.isPhotoAtTempo()) {
            this.metronome.setPhotoAtTempo(false);
            if (this.takenPicturesNum == 0)
                ProcessImage.takeReferencePicture(mRgba);
            else {
                ProcessImage.takeComparatorPicture(mRgba);
                playSoundForMaterial(ProcessImage.checkInteraction());
            }
            this.takenPicturesNum++;
        }
        return mRgba;
    }

    private void findCodeOnImage(Mat image) {
        alternateFlashlight();
        String code = QRCodeReader.decodeQRCode(mRgba);
        if (code == null || code.equals(""))
            this.checkCodeAttempts++;
        else {
            this.checkCodeAction = false;
            this.checkCodeAttempts = 0;
            mOpenCvCameraView.turnOffFlash();
            Intent verifyIntent = new Intent(this, SoundDownloader.class);
            verifyIntent.putExtra("code",code);
            startService(verifyIntent);
        }
    }
    private void alternateFlashlight() {
        if (this.checkCodeAttempts > this.maxCheckCodeAttempts) {
            this.checkCodeAttempts = 0;
            if (mOpenCvCameraView.isFlashEnabled()) mOpenCvCameraView.turnOffFlash();
            else mOpenCvCameraView.turnOnFlash();
            try { Thread.sleep(1000); }
            catch (InterruptedException e) { e.printStackTrace(); }
        }
    }

    private void playSoundForMaterial(boolean[] interaction) {
        Intent plays = new Intent(this, PlaySound.class);
        if (countInteraction(interaction) > 1) {
            plays.putExtra("sound","/wrong_interaction.mp3");
            startService(plays);
        }
        else if (countInteraction(interaction) == 1) {
            if (interaction[0]) plays.putExtra("sound","/sound1.mp3");
            if (interaction[1]) plays.putExtra("sound","/sound2.mp3");
            if (interaction[2]) plays.putExtra("sound","/sound3.mp3");
            if (interaction[3]) plays.putExtra("sound","/sound4.mp3");
            startService(plays);
        }
    }
    private int countInteraction(boolean[] interaction) {
        int count = 0;
        if (interaction[0]) count++;
        if (interaction[1]) count++;
        if (interaction[2]) count++;
        if (interaction[3]) count++;
        return count;
    }


    /**
     *  Menu settings
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.enableDisableViewOption:
                this.viewEnabled = !this.viewEnabled;
                recreate();
                break;
            case R.id.sheetCodeId:
                if (this.checkCodeAction) {
                    this.mOpenCvCameraView.turnOffFlash();
                    this.checkCodeAttempts = 0;
                }
                this.checkCodeAction = !this.checkCodeAction;
                break;
            case R.id.unlinkSheetCodes:
                Intent unlinkIntent = new Intent(this, CodeUnlinker.class);
                turnOffGame();
                startService(unlinkIntent);
                break;
            case R.id.settingsOption:
                Intent settingIntent = new Intent(this, SetPreferencesActivity.class);
                startActivityForResult(settingIntent, 0);
                break;
            case R.id.helpOption:
                Intent helpIntent = new Intent(this, HelpActivity.class);
                startActivity(helpIntent);
                break;
            case R.id.exitOption:
                android.os.Process.killProcess(android.os.Process.myPid());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Service Callbacks (Function called when invoked service finished).
     */
    public static final String SERVICE_GET_SOUNDS_ACTION = "getSoundsAction";
    public static final String SERVICE_GET_SOUNDS_DATA = "getSoundsResult";
    public static final String SERVICE_UNLINK_CODES_ACTION = "unlinkCodesAction";
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String serviceToStop = intent.getAction();
            if (serviceToStop.equals(SERVICE_GET_SOUNDS_ACTION)) {
                if (intent.getBooleanExtra(SERVICE_GET_SOUNDS_DATA,false)) turnOnGame();
                Intent stopIntent = new Intent(GameActivity.this, SoundDownloader.class);
                stopService(stopIntent);
            }
            if (intent.getAction().equals(SERVICE_UNLINK_CODES_ACTION)) {
                Intent stopIntent = new Intent(GameActivity.this, CodeUnlinker.class);
                stopService(stopIntent);
            }
        }
    };


}
