package com.example.david.tactilemusicsense.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class ApplicationPermission {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 202;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 303;

    private static Context context;
    private static Activity activity;

    public static void init(Activity activity) {
        ApplicationPermission.activity = activity;
        context = activity.getApplicationContext();
        requestPermissionByName(Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA);
        requestPermissionByName(Manifest.permission.INTERNET, MY_PERMISSIONS_REQUEST_INTERNET);
    }

    private static void requestPermissionByName(String permission, int requestId) {
        if (!checkPermissionByName(permission)) {
            ActivityCompat.requestPermissions(
                    activity,
                    new String[]{permission},
                    requestId);
        }
    }
    private static boolean checkPermissionByName(String permission){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        return true;
    }
}
