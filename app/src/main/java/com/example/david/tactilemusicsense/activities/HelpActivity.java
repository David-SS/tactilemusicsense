package com.example.david.tactilemusicsense.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.VideoView;
import com.example.david.tactilemusicsense.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HelpActivity extends AppCompatActivity {

    private List<Integer> videoList;
    private int activatedVideo;
    private IntentFilter mIntentFilter;
    public final static String HIDE_VIDEO_PANEL_ACTION = "hideVideoPanelAction";
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(HIDE_VIDEO_PANEL_ACTION)) hideVideoPanel();
        }
    };

    private Timer hideTimer;
    private MainTask hideTask;
    class MainTask extends TimerTask {
        @Override
        public void run() {
            Intent hideVideoPanel = new Intent();
            hideVideoPanel.setAction(HelpActivity.HIDE_VIDEO_PANEL_ACTION);
            sendBroadcast(hideVideoPanel);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        this.mIntentFilter = new IntentFilter();
        this.mIntentFilter.addAction(HIDE_VIDEO_PANEL_ACTION);

        setVideoOnTouchListener();
        setVideoOnCompletionListener();

        addVideoList();
        setVideoContent(R.raw.v1);
        setInitialFrame();
    }
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setVideoOnTouchListener() {
        VideoView videoView = findViewById(R.id.helpVideoView);
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (getVideoPanelState()) hideVideoPanel();
                else showVideoPanel();
                return false;
            }
        });
    }
    private void setVideoOnCompletionListener() {
        VideoView videoView = findViewById(R.id.helpVideoView);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer vmp) {
                VideoView videoView = findViewById(R.id.helpVideoView);
                videoView.seekTo(0);
                while(!videoView.isPlaying()) videoView.start();
            }
        });
    }

    private void addVideoList() {
        this.videoList = new ArrayList<>();
        this.activatedVideo = 0;
        videoList.add(R.raw.v1);
        videoList.add(R.raw.v2);
        videoList.add(R.raw.v3);
        videoList.add(R.raw.v4);
        videoList.add(R.raw.v5);
    }
    private void setVideoContent(int videoId){
        VideoView videoView = findViewById(R.id.helpVideoView);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+videoId);
        videoView.setVideoURI(uri);
    }
    private boolean getVideoPanelState() {
        LinearLayout videoPanel = findViewById(R.id.linearLayoutVideoPanel);
        int visibility = videoPanel.getVisibility();
        if (visibility == LinearLayout.INVISIBLE) return false;
        else return true;
    }

    private void showVideoPanel() {
        LinearLayout videoPanel = findViewById(R.id.linearLayoutVideoPanel);
        videoPanel.setVisibility(LinearLayout.VISIBLE);
        restartTimer();
    }
    private void hideVideoPanel() {
        LinearLayout videoPanel = findViewById(R.id.linearLayoutVideoPanel);
        videoPanel.setVisibility(LinearLayout.INVISIBLE);
        stopTimer();
    }
    private void startTimer() {
        hideTimer = new Timer();
        hideTask = new MainTask();
        hideTimer.schedule(hideTask, 2500, 2500);
    }
    private void stopTimer() {
        if (hideTimer != null && hideTask != null) {
            hideTimer.cancel();
            hideTask.cancel();
        }
    }
    private void restartTimer() {
        stopTimer();
        VideoView videoView = findViewById(R.id.helpVideoView);
        if (videoView.isPlaying()) startTimer();
    }

    public void playPause(View view) {
        VideoView videoView = findViewById(R.id.helpVideoView);
        ImageButton playPause = findViewById(R.id.playPauseButton);
        if (videoView.isPlaying()) {
            playPause.setImageResource(R.drawable.ic_play_arrow_white_50dp);
            videoView.pause();
        }
        else {
            playPause.setImageResource(R.drawable.ic_pause_white_50dp);
            videoView.start();
        }
        restartTimer();
    }
    public void restart(View view) {
        VideoView videoView = findViewById(R.id.helpVideoView);
        videoView.seekTo(0);
        videoView.start();

        ImageButton playPause = findViewById(R.id.playPauseButton);
        playPause.setImageResource(R.drawable.ic_pause_white_50dp);

        restartTimer();
    }
    public void back(View view) {
        if (this.videoList.size() > 1) {
            int backVideo = this.activatedVideo - 1;
            if (backVideo == -1) backVideo = videoList.size()-1;

            this.activatedVideo = backVideo;
            setVideoContent(videoList.get(backVideo));
            setInitialFrame();
        }
        restartTimer();
    }
    public void next(View view) {
        if (this.videoList.size() > 1) {
            int backVideo = this.activatedVideo + 1;
            if (backVideo == videoList.size()) backVideo = 0;

            this.activatedVideo = backVideo;
            setVideoContent(videoList.get(backVideo));
            setInitialFrame();
        }
        restartTimer();
    }
    private void setInitialFrame() {
        VideoView videoView = findViewById(R.id.helpVideoView);
        videoView.seekTo(100);
        videoView.start();
        videoView.pause();

        ImageButton playPause = findViewById(R.id.playPauseButton);
        playPause.setImageResource(R.drawable.ic_play_arrow_white_50dp);
    }

    /**
     * To recreate back button action on back toolbar button
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
