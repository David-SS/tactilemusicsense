package com.example.david.tactilemusicsense.activities.preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.example.david.tactilemusicsense.R;
import com.example.david.tactilemusicsense.activities.CameraCalibration;

public class PreferencesFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_BPM = "pref_key_metronome_bpm";
    public static final String KEY_TEMPO = "pref_key_metronome_tempo";
    public static final String KEY_WAIT_TEMPO = "pref_key_metronome_wait_tempo";
    public static final String KEY_FLASH = "pref_key_interaction_flash";
    public static final String KEY_SENSITIVITY = "pref_key_interaction_sensitivity";
    public static final String KEY_CALIBRATE_CAMERA = "pref_key_calibrate_camera";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        addIntentDisplayActions();
    }

    private void addIntentDisplayActions() {
        Preference calibrateCameraPref = (Preference) findPreference(KEY_CALIBRATE_CAMERA);
        calibrateCameraPref.setOnPreferenceClickListener (new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent calibrate_camera_intent = new Intent(getActivity().getApplicationContext(), CameraCalibration.class);
                startActivity(calibrate_camera_intent);
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //activate listener to detect when any preference change
        getPreferenceScreen().getSharedPreferences().
                registerOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onPause() {
        super.onPause();
        //deactivate listener to stop detect events
        getPreferenceScreen().getSharedPreferences().
                unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Toast.makeText(getActivity().getApplicationContext(),"Valor actualizado", Toast.LENGTH_SHORT).show();
    }


    /**
     * Read string preference actual value outside Setting Screen
     * @param context
     * @param key
     * @return
     */
    public static String getString(Context context, final String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key,"");
    }

    /**
     * Write string preference value outside Settings Screen
     * @param context
     * @param key
     * @param value
     */
    public static void setString(Context context, final String key, final String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key,value);
        editor.commit();
    }

    /**
     * Read boolean preference actual value outside Setting Screen
     * @param context
     * @param key
     * @return
     */
    public static boolean getBoolean(Context context, final String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(key,false);
    }

    /**
     * Write string preference value outside Settings Screen
     * @param context
     * @param key
     * @param value
     */
    public static void setBoolean(Context context, final String key, final boolean value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }
}