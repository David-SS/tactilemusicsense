package com.example.david.tactilemusicsense.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.example.david.tactilemusicsense.R;
import com.example.david.tactilemusicsense.activities.preferences.PreferencesFragment;
import com.example.david.tactilemusicsense.camera.JavaCameraClass;
import com.example.david.tactilemusicsense.imageProcessing.ProcessImage;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class CameraCalibration extends AppCompatActivity implements CvCameraViewListener2 {

    private JavaCameraClass mOpenCvCameraView;
    private Mat mRgba;

    public CameraCalibration() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_camera_calibration);

        mOpenCvCameraView = (JavaCameraClass) findViewById(R.id.javacameraclass_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setHasFlash(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    mOpenCvCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mOpenCvCameraView.setDefaultAutoFocusMode();
        mOpenCvCameraView.doManualFocus();
        Boolean flash = PreferencesFragment.getBoolean(this,PreferencesFragment.KEY_FLASH);
        if (flash) mOpenCvCameraView.turnOnFlash();
    }
    public void onCameraViewStopped() { mRgba.release(); }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        ProcessImage.drawCross(mRgba);
        ProcessImage.drawSquare(mRgba);

        return mRgba;
    }
}